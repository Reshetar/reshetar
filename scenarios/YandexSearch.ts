import MainPage = require("../page_objects/MainPage");
import ResultPage = require("../page_objects/ResultPage");
import {browser} from "protractor";
/**
 * Created by VolodymyrParlah on 1/16/17.
 */
describe("First result contains QA Automation in the link", function () {

    var  mainPage = new MainPage();
    let resultPage  = new ResultPage();

    it("Main page is opened", function () {
        browser.get('/');
        expect(mainPage.getPageTitle()).toContain("Яндекс", "Wrong title")
    });
    it("First result contains QA Automation", function () {
        mainPage.typeTextToSearchField("QA Automation");
        resultPage.ClickOnSearchIcon();
        expect(resultPage.GetFirstItemTitle()).toContain("QA Automation", "Uri does not contain QA Automation");
    });
    it("Result count", function () {
        expect(resultPage.ResultCount()).toBe(10, "Wrong Result");
    });
});


