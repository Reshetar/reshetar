import PageAbstract = require("./BasicPage");
import {element, by, browser, ExpectedConditions} from "protractor";
/**
 * Created by VolodymyrParlah on 1/16/17.
 */
class ResultPage extends PageAbstract{

    private SearchIcon = element(by.css('.suggest2-form__button'));
    private ResultArea = element(by.css('.serp-list_left_yes'));
    private AllResultItem = this.ResultArea.all(by.css('.serp-item'));

    public ClickOnSearchIcon(){
        this.SearchIcon.click();
    }

    public GetFirstItemTitle(){
        browser.wait(ExpectedConditions.visibilityOf(this.ResultArea), 9000);
        return this.AllResultItem.last().element(by.xpath('//ul/li[1]/div/h2/a')).getText();
    }

    public ResultCount(){
        return this.AllResultItem.count();
    }
}
export = ResultPage;