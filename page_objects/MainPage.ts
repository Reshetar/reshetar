import PageAbstract = require("./BasicPage");
import {element, by} from "protractor";
/**
 * Created by VolodymyrParlah on 1/16/17.
 */
class MainPage extends PageAbstract{

    private SearchField = element(by.css('#text'));


    public typeTextToSearchField(text: string){
        this.SearchField.sendKeys(text);
    }

}
export = MainPage;